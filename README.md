![제목 없음.jpg](https://bitbucket.org/repo/e88rE9/images/2754784105-%EC%A0%9C%EB%AA%A9%20%EC%97%86%EC%9D%8C.jpg)
Tweepy를 이용해 만든 아-주 간단한 (첫 커밋 당시 67줄!) 코드입니다.
1) 로그인
2) 타임라인 보기
3) 트윗 보내기

기능이 구현되어 있습니다.

제 consumer_key와 consumer_secret이 빠져있기 때문에, pull하신 뒤 CLI_twitter.py가 있는 경로에 config.txt를 만들어주신 뒤 다음의 형식으로 키를 채워주셔야 합니다.

consumer_key=
consumer_secret=
access_token_key=
access_token_secret=

액세스 토큰은 없어도 문제없이 작동하며 (웹페이지로 리다이렉트 되어서 PIN을 물어볼 겁니다.) 수동으로 config.txt에 입력해주시면 테스트 할 때마다 pin을 입력하는 귀찮음을 피하실 수 있습니다.
(token = auth.get_access_token(verifier=pin) 에서 token의 0번이 access_token_key, 1번이 secret입니다. 수동으로 입력해주세욥. 자동 입력 제가 귀찮아서 구현 안함 > <)

colorama.init()은 공식 라이브러리가 아니라서 OS가 달라지거나 python version이 달라지면 malfunction할 수 있습니다. 그러면 주석처리 해주세요. 어차피 colorama, termcolor는 색깔처리 하는 라이브러리라서 없어도 상관없습니다. 순혈푸티는 흑백이라구요!

Spec:
lang: Python 2.7.9
necessary external libraries: tweepy
깔면 좋은 라이브러리: termcolor, colorama (for coloring.)
*맥이나 리눅스에서는 colorama를 안깔아도 된다고 하는 것 같기도 하구요.

감사합니다.

14-12-21