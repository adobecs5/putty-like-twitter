#-*- coding: utf-8 -*-
from termcolor import colored
import colorama
import tweepy
import webbrowser
from getpass import getpass
global api
colorama.init()  #if there is an error in Visual Studio or python 64-bit, comment out this line.

def initiate():
    global api
    print "login as: admin"
    try:
        f = open ('config.txt', 'r')
        lines = f.readlines()
        consumer_key = lines[0].split('=')[1][:-1]
        consumer_secret = lines[1].split('=')[1][:-1]
        f.close()
    except:
        raise Exception("please set consumer key and consumer secret in config.txt")
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    try:
        f = open ('config.txt', 'r')
        lines = f.readlines()
        access_token_key = lines[2].split('=')[1][:-1]
        access_token_secret = lines[3].split('=')[1]#if there is an enter(\n) at the end of the 4th line, add [:-1].
        f.close()
        auth.set_access_token(access_token_key, access_token_secret)
        print 'admin@192.168.0.1\'s password: '
    except:
        webbrowser.open(auth.get_authorization_url())
        pin = getpass('admin@192.168.0.1\'s password: ').strip()
        token = auth.get_access_token(verifier=pin)
        auth.set_access_token(token[0], token[1])

    api = tweepy.API(auth)
    print "Last login: Sun Dec 21 19:53:45 2014 from 192.168.0.1"

#api.update_status('Update Status Test #Tweepy')
def update_timeline():
    global api
    public_tweets = api.home_timeline()
    for i in range (5):
        tweet = public_tweets[i]
        print colored(tweet.author.name, 'green') +"@"+ tweet.author.screen_name
        print tweet.text

def tweet(string):
    global api
    api.update_status(string)

initiate()
while (1):
    string = "["+ colored("root","magenta") +"@localhost twitterCLI]# "
    order = raw_input (string)
    order = order.split(" ")
    if order[0] == 'exit':
        break
    elif order[0] == 'timeline':
        update_timeline()
    elif order[0] == 'tweet':
        print "Upload tweet:%s"%order[1]
        if (raw_input(colored('(Y/N)', 'red') + ">>").lower() == 'y'):
            tweet(order[1])
    else:
        print "-bash: %s: command not found"%order[0]
    print ""